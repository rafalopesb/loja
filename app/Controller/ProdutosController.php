<?php 
class ProdutosController extends AppController{

    public $components = array('ComportamentoPadraoForm');
    public $helpers = array('Money');
    public $paginate = array(
        'limit' => parent::defaultLimitTable,
        'order' => array(
            'Produto.id' => 'asc'
        )
    );

    public function index(){
        
        $data = $this->paginate('Produto');
        
        $this->set('produtos', $data);
    }

    public function edit($id){

        $Categorias = ClassRegistry::init('Categorias');
        $this->set('options_categoria', $this->Produto->getArraySelectCategorias($Categorias->find('all')));
        $this->ComportamentoPadraoForm->edit($id);
    }

    public function add(){

        $Categorias = ClassRegistry::init('Categorias');
        $this->set('options_categoria', $this->Produto->getArraySelectCategorias($Categorias->find('all')));
        $this->ComportamentoPadraoForm->add();
    }

    public function delete($id){
        $this->ComportamentoPadraoForm->delete($id);
    }

    public function preview_product(){
        $this->layout = "layout_loja";
        $id = $this->request->params['id'];

        $this->set('produto', $this->Produto->findById($id));
    }
}
?>