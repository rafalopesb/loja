<?php 

class CategoriasController extends AppController {

    public $components = array('ComportamentoPadraoForm');
    public $paginate = array(
        'limit' => parent::defaultLimitTable,
    );

    public function index(){
        $this->set('categorias', $this->paginate('Categoria'));
    }

    public function edit($id){
        $this->ComportamentoPadraoForm->edit($id);
    }

    public function add(){
        $this->ComportamentoPadraoForm->add();
    }

    public function delete($id){
        $this->ComportamentoPadraoForm->delete($id);
    }

    public function preview_category(){
        $id = $this->request->params['id'];
        
        $this->layout = "layout_loja";
        
        $this->set('categoria', $this->Categoria->find('first', array('conditions'=> array('Categoria.id'=> $id))));
        $this->set('lista_categorias', $this->Categoria->find('all', array('limit'=> 12)));
    }

}
?>