<?php 

class UsersController extends AppController{

    public function beforeFilter(){
        parent::beforeFilter();

        $this->Auth->allow(array("novo"));
        $this->layout = "layout_loja";
    }

    public function novo(){
        
        if($this->request->is('post')){

            if($this->User->save($this->request->data['User'])){
                $this->Flash->success('Usuário salvo com sucesso');        
            }

            return $this->redirect(array('controller'=> 'Users', 'action'=> 'novo'));
        }
    }

    public function login(){
        
        if($this->request->is('post')){
            
            if($this->Auth->loggedIn()){
                return $this->redirect($this->Auth->loginRedirect);
            }

            if( $this->Auth->login() ){
                return $this->redirect($this->Auth->loginRedirect);
            }else{
                $this->Flash->error('Usuário ou senha inválidos');        
            }
        }else{
            $this->Auth->logout();
        }
        
    }

    public function logout(){
        $this->Auth->logout();
        
        return $this->redirect(array('action'=> 'login'));
    }
}

?>