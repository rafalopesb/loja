<?php 

class ComportamentoPadraoFormComponent extends Component{

    function startup(Controller $controller) {
        $this->controller = $controller; 
    }

    public function edit($id){
        
        $this->controller->set('id', $id);
        
        if($this->controller->request->is('get')){
            $this->controller->request->data = $this->controller->{$this->controller->modelClass}->findById($id);
        }else{
            
            if($this->controller->{$this->controller->modelClass}->saveAll($this->controller->request->data)){
                return $this->controller->redirect(array('action'=> 'index'));
            }

            $this->controller->Flash->set('Ocorreu um erro na edição');
        }
    }

    public function add(){
        
        $this->controller->set('id', false);
        
        if(!$this->controller->request->is('get')){
            
            if($this->controller->{$this->controller->modelClass}->save($this->controller->request->data)){
                return $this->controller->redirect(array('action'=> 'index'));
            }

            $this->controller->Flash->set('Ocorreu um erro na edição');
        }

        $this->controller->render('edit');
    }

    public function delete($id){

        if($this->controller->{$this->controller->modelClass}->delete($id)){
            $this->controller->Flash->success('Deletado com sucesso');
        }else{
            $this->controller->Flash->set('Ocorreu um erro na exclusão');
        }

        
        return $this->controller->redirect(array('action'=> 'index'));
    }
}
?>