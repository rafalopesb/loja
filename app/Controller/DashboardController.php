<?php 
    class DashboardController extends AppController{

        public $helpers = array("Money");
        public function index(){
            $this->layout = 'layout_loja';

            $dados = $this->Dashboard->getScreenData();
            
            $this->set('produtos', $dados['produtos']);
            $this->set('categorias', $dados['categorias']);
        }
    }
?>