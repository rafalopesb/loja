<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 form-group">
    <div class="product border">
        <a href="/loja/produtos/<?=$produto['id']?>">
            <div class="product-img">
                <?php echo $this->Html->image('produtos/' . $produto['imagem'], array('width' => '140px', 'alt' => $produto['descricao'])); ?>
            </div>

            <div class="product-block">
                <h5 class="truncate">
                    <?=$produto['descricao']; ?>
                </h5>
                <h5 class="text-danger">
                    <?=$this->money->maskMoney(floatval($produto['preco']));?>
                </h5>
                <ul class="list-inline ">
                    <li class="list-inline-item">
                        <i class="fa fa-credit-card"></i> 12x de
                        <?=$this->money->maskMoney(floatval($produto['preco']) / 12);?>
                    </li>
                </ul>
            </div>
        </a>
        <div class="product-footer">
            <div class="row">
                <div class="col-md-12">
                    <?php if ($produto['frete_gratis']) {?>
                    <i class="fa fa-truck" aria-hidden="true"></i>
                    <b style="color: green">Frete Grátis</b>
                    <?php } else {?>
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <b style="color: orange">10% off</b>
                    <?php }?>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-outline-secondary btn-sm">Contact Seller</button>
                </div>
            </div>
        </div>
    </div>
</div>