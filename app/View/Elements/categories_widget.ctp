<h1><?= $title ?></h1>

<div class="row mt-3 mb-4 text-center form-group">
    <div class="col-md-12">
        <?php  foreach($categorias as $cat){
            $cat['Categorias'] = isset($cat['Categorias']) ? $cat['Categorias'] : $cat['Categoria']; //corrige o nome errado dado a classe no singular
        ?>
        <div class="col-md-3 col-sm-3 col-xs-6 category-container">
            <div class="category border">
                <i class="fa fa-<?= $cat['Categorias']['icon']?>" aria-hidden="true"></i>
                <a href="/loja/categorias/<?= $cat['Categorias']['id']?>">
                    <span class="category-name">
                        <?= utf8_encode($cat['Categorias']['nome']) ?>
                    </span>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>