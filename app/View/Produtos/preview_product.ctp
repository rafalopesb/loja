<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<?php echo $this->Html->css('preview_product'); ?>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<?php echo $this->Html->script('preview_product'); ?>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    <body>
        <div class="container">
        	<div class="row">
               <div class="col-xs-4 item-photo">
                <?php echo $this->Html->image('produtos/'.$produto['Produto']['imagem'], array('max-width' => '100%','alt'=> $produto['Produto']['descricao'])); ?>
                </div>
                <div class="col-xs-5" style="border:0px solid gray">
                    
                    <h3><?= $produto['Produto']['descricao'] ?></h3>    
                    <h5 style="color:#337ab7">vendido por <a href="#">LojaX</a> · <small style="color:#337ab7">(999 vendas)</small></h5>
        
                    <!-- Precios -->
                    <h6 class="title-price"><small>PREÇO OFERTA</small></h6>
                    <h3 style="margin-top:0px;"><?= $this->money->maskmoney(floatval($produto['Produto']['preco'])); ?></h3>
        
                    <!-- Detalles especificos del producto -->
                    <div class="section">
                        <h6 class="title-attr" style="margin-top:15px;" ><small>COLOR</small></h6>                    
                        <div>
                            <div class="attr" style="width:25px;background:#5a5a5a;"></div>
                            <div class="attr" style="width:25px;background:white;"></div>
                        </div>
                    </div>
                    <div class="section" style="padding-bottom:5px;">
                        <!-- <h6 class="title-attr"><small>CAPACIDAD</small></h6>                    
                         <div>
                            <div class="attr2">16 GB</div>
                            <div class="attr2">32 GB</div>
                        </div>  -->
                    </div>   
                    <div class="section" style="padding-bottom:20px;">
                        <h6 class="title-attr"><small>CANTIDAD</small></h6>                    
                        <div>
                            <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                            <input value="1" />
                            <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
                        </div>
                    </div>                
        
                    <!-- Botones de compra -->
                    <div class="section" style="padding-bottom:20px;">
                        <button id="adicionar-ao-carrinho" data-id="<?= $produto['Produto']['id']; ?>" class="btn btn-success"><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Adicionar ao carrinho</button>
                        <h6><a href="#"><span class="glyphicon glyphicon-heart-empty" style="cursor:pointer;"></span> Adicionar a lista de desejos</a></h6>
                    </div>                                        
                </div>                              
        
                <div class="col-xs-9">
                    <ul class="menu-items">
                        <li class="active">Detalhe do produto</li>
                        <li>Garantia</li>
                        <li>Vendedor</li>
                        <li>Envio</li>
                    </ul>
                    <div style="width:100%;border-top:1px solid silver">
                        <p style="padding:15px;">
                            <small>
                            <?= $produto['Produto']['descricao']; ?>
                            </small>
                        </p>
                        <small>
                            <ul>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                                <li>Característica do produto</li>
                            </ul>  
                        </small>
                    </div>
                </div>		
            </div>
        </div>        
    </body>
</html>
