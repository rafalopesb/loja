<h1><?= is_numeric($id) ? "Edição de " : "Cadastro de" ?> Produto</h1>    

<?php 
    
    echo $this->Form->Create('Produto', array(
        'enctype' => 'multipart/form-data'
    ));
    echo $this->Form->input('descricao');
    echo $this->Form->input('preco');
    echo $this->Form->input('id', array('type'=> 'hidden'));
    echo $this->Form->select('categoria_id', isset($options_categoria) ? 
        $options_categoria :  [], array('default'=> isset($this->data['Categoria'][0]) ? $this->data['Categoria'][0]['id'] : 0 ));
    echo $this->Form->input('imagem', ['type' => 'file']);
    echo $this->Form->end('Salvar');
?>