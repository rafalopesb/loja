<div>
    <h1 style="display: inline">Produtos</h1> -
    <b style="display: inline"   ><?= $this->Html->link('Novo', array('action'=> 'add')) ?></b>
</div>
<div>
    <table>
        <thead>
            <th>ID</th>
            <th>Descrição</th>
            <th>Valor</th>
            <th></th>
            <th></th>
        </thead>
        <tbody>
            <?php
                foreach($produtos as $prd){ ?>
                    <tr>
                        <td><?= $prd['Produto']['id'] ?></td>
                        <td><?= $prd['Produto']['descricao'] ?></td>
                        <td><?= $this->Money->maskMoney($prd['Produto']['preco']); ?></td>
                        <td><?= $this->Html->link('Editar', array('action'=> 'edit', $prd['Produto']['id'])) ?></td>
                        <td><?= $this->Form->postLink('Deletar', 
                            array('action'=> 'delete', $prd['Produto']['id']),
                            array('confirm' => 'Tem certeza que deseja deletar a categoria?')
                            ) ?></td>
                    </tr>
                <?php }
            ?>
        </tbody>
    </table>
    <?=  $this->Paginator->numbers(array('first' => 'First page')) ?>
    
</div>