<h1><?= is_numeric($id) ? "Edição de " : "Cadastro de" ?> Categoria</h1>    
<?php
    echo $this->Form->create('Categoria');
    echo $this->Form->input('nome');
    echo $this->Form->input('icon');
    echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->end('Salvar Categoria');
?>