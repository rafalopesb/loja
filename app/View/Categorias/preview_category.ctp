<h1 style="color: #6ba0da !important">
    <i class="fa fa-<?= $categoria['Categoria']['icon'] ?>"></i>
    <?= $categoria['Categoria']['nome'] ?>
</h1>

<?php if(empty($categoria['Produto'])){ ?>
    <span>Nenhum produto encontrado na categoria <?= $categoria['Categoria']['nome'] ?> </span>
<?php }else{ 
        $this->Helpers->load('Money');
        foreach($categoria['Produto'] as $produtos){ 
            echo $this->element('default_product_container', array('produto'=> $produtos), array('helpers'=> 'money'));   
        }
    }
    
    
?>

 <div class="col-md-12">
    <?=  $this->element('categories_widget', array('categorias'=> $lista_categorias, 'title'=> 'Outras categorias')); ?>
</div>
