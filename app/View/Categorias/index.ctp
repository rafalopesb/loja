<div>
    <h1 style="display: inline">Categorias</h1> -
    <b style="display: inline"   ><?= $this->Html->link('Novo', array('action'=> 'add')) ?></b>
</div>
<div>
    <table>
        <thead>
            <th>ID</th>
            <th>Descrição</th>
            <th></th>
            <th></th>
        </thead>
        <tbody>
            <?php
                foreach($categorias as $cat){ ?>
                    <tr>
                        <td><?= $cat['Categoria']['id'] ?></td>
                        <td><?= $cat['Categoria']['nome'] ?></td>
                        <td><?= $this->Html->link('Editar', array('action'=> 'edit', $cat['Categoria']['id'])) ?></td>
                        <td><?= $this->Form->postLink('Deletar', 
                            array('action'=> 'delete', $cat['Categoria']['id']),
                            array('confirm' => 'Tem certeza que deseja deletar a categoria?')
                            ) ?></td>
                    </tr>
                <?php }
            ?>
        </tbody>
    </table>
    <?=  $this->Paginator->numbers(array('first' => 'First page')) ?>
</div>