<?php

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('layout_loja');

		echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->css('/libs/bootstrap/css/bootstrap.css');
        echo $this->fetch('script');    
        echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js');
        echo $this->Html->script('/libs/bootstrap/js/bootstrap.js');
        echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css');
	?>
</head>
<body>
    
	<div id="container">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header"> 
                    <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">
                        <?php echo $this->Html->image('tray-novamarca.png', array('width' => '140px','alt'=> 'Tray')); ?>
                    </a> 
                </div>
                
                <?php if(!empty(AuthComponent::user())){ ?>
                    <ul class="nav navbar-nav">
                        <li><?= $this->Html->link('Home', '/dashboard') ?></li>
                     </ul>
                 <?php } ?>

                <div class="pull-right">

                    <?php if(!empty(AuthComponent::user())){ ?>
                        <div class="dropdown">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="/loja/carrinho/checkout"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= AuthComponent::user('username') ?><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Configurações</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><?= $this->Html->link('Logout', '/users/logout') ?></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </nav>
		<div id="content">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
