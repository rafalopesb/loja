<?php 
    class MoneyHelper extends AppHelper{

        public function maskMoney($value){
            return 'R$ ' . number_format($value, 2, ',', '.');
        }
    }
?>