<div>
    <h1>Produtos</h1>
    <div class="row mt-3 mb-4 text-center">
        <?php if(is_array($produtos) && !empty($produtos)){
            foreach($produtos as $prd){
            echo $this->element('default_product_container', array('produto'=> $prd['Produto']));
        }
     }else{
        echo "Nenhum produto cadastrado";
     } ?>
    </div>

    <hr>
    
    <?= $this->element('categories_widget', array('categorias'=> $categorias, 'title'=> 'Categorias')); ?>

    <ul class="list-group" style="margin: 0px !important">
        <li class="list-group-item" style="margin-right: 0px !important; margin-left: 0px !important"><?= $this->Html->link('Lista de produtos', '/produtos') ?></li>
        <li class="list-group-item" style="margin-right: 0px !important; margin-left: 0px !important"><?= $this->Html->link('Lista de categorias', '/categorias') ?></li>
    </ul>
    
</div>