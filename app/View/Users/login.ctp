<div class="col-md-6 center">
<?php 
	echo $this->Html->css('/css/login.css');
    echo $this->Form->create('User', array('url'=> "/users/login", 'class'=>"well"));
    echo $this->Form->Input('username', array('class'=> 'form-control'));
    echo $this->Form->Input('password', array('class'=> 'form-control'));
    echo $this->Form->submit('Login');
    echo $this->Form->end();
?>
</div>