
<div class="container">
	<div class="well">
		<h1>Cadastro de usuário</h1>
		<?php 
			echo $this->Html->css('/css/login.css');
		    echo $this->Form->create('User', array('url'=> "/users/novo"));
		    echo $this->Form->Input('username', array('class'=> 'form-control'));
		    echo $this->Form->Input('password', array('class'=> 'form-control'));
		    echo $this->Form->submit('Salvar');
		    echo $this->Form->end();
		?>
	</div>
</div>