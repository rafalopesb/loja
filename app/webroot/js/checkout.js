$(document).ready(function(){

    $('.removeitemCarrinho').click(function(){

        var self = this;
        $.ajax({
            url: "/loja/carrinho/checkout/removeProductFromCart",
            dataType: "json",
            type: "post",
            data: {
                "id": $(this).attr('data-id')
            },
            success: function(data){
                
                if(data.status){
                    
                    $(self).closest('.item-cart').next('hr').remove();
                    $(self).closest('.item-cart').remove();
                    alert(data.msg);
                }

                if($("#content .panel-body .item-cart").length == 0){
                    $("#content .panel-body").prepend('<b>Nenhum Produto adicionado ao carrinho</b>')
                }
            }
        });
    });
})
