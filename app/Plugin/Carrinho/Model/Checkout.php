<?php 
    class Checkout extends AppModel{
        
        public $useTable = false;

        public function addProductInSession($dados_produto){
            $produtos_sessao = CakeSession::read('produtos');
            $produtos_sessao[] = $dados_produto;
            
            CakeSession::write('produtos', $produtos_sessao);
        }

        public function getProduct($id){
            $produto = ClassRegistry::init('Produtos');
            return $produto->findById($id);
        }

        public function removeProductFromCart($id){
            $produtos_sessao = CakeSession::read('produtos');
            $produto_encontrado = false;

            foreach($produtos_sessao as $key=> $prd){
                if($prd['Produtos']['id'] == $id){
                    unset($produtos_sessao[$key]);
                    $produto_encontrado = true;
                }
            }

            CakeSession::write('produtos', $produtos_sessao);
            return $produto_encontrado;
        }

        public function produtoJaAdicionado($produto){
            $produtos_sessao = CakeSession::read('produtos');
            $ja_adicionado = false;

            foreach($produtos_sessao as $key=> $prd){
                if($prd['Produtos']['id'] == $produto['Produtos']['id'] ){
                    $ja_adicionado = true;
                    break;
                }
            }

            return $ja_adicionado;
        }
    }
?>