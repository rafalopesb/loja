<?php 
    class CheckoutController extends CarrinhoAppController{
    
        public function index(){
            $this->layout = 'layout_loja';
            
            $produtos_sessao = CakeSession::read('produtos');
            
            $this->set('produtos', $produtos_sessao);
        }

        public function addProductToCart(){
            
            $this->autoRender = false;
            $dados_produto = $this->Checkout->getProduct($this->request->data('id'));
            
            if($this->Checkout->produtoJaAdicionado($dados_produto)){
                echo json_encode(array('status'=> false, 'msg'=> 'Produto já adicionado, verifique o carrinho'));
                return;
            }

            if(is_array($dados_produto['Produtos'])){
                $this->Checkout->addProductInSession($dados_produto);

                echo json_encode(array('status'=> true, 'msg'=> 'Produto adicionado ao carrinho'));
            }else{
                echo json_encode(array('status'=> false, 'msg'=> 'Erro ao adicionar o produto ao carrinho'));
            }
        }

        public function removeProductFromCart(){
            $this->autoRender = false;
            $status = $this->Checkout->removeProductFromCart($this->request->data('id'));
            
            if($status == true){
                echo json_encode(array('status'=> true, 'msg'=> 'Produto removido do carrinho'));
                return;
            }
            
            echo json_encode(array('status'=> false, 'msg'=> 'Erro ao remover o produto do carrinho'));
            
        }
    }
?>