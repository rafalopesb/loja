<style>
    #content, #container{
        background: white !important;
    }
</style>
<?php echo $this->Html->script('checkout'); ?>
<div class="container">
    <div class="panel-body">
        
        <?php 
            if(is_array($produtos) && count($produtos) > 0){
                $this->Helpers->load('Money');
                foreach($produtos as $produto){ ?>
                <div class="row item-cart">
                    <div class="col-xs-2">
                        <?php echo $this->Html->image('produtos/' . $produto['Produtos']['imagem'], array('width' => '140px', 'alt' => $produto['Produtos']['descricao'])); ?>
                    </div>
                    <div class="col-xs-4">
                        <h4 class="product-name">
                            <strong><?=$produto['Produtos']['descricao']; ?></strong>
                        </h4>
                        <h4>
                            <small>Product description</small>
                        </h4>
                    </div>
                    <div class="col-xs-6">
                        <div class="col-xs-6 text-right">
                            <h6>
                                <strong>
                                    <?= $this->money->maskMoney(floatval($produto['Produtos']['preco']));?>
                                    <span class="text-muted">x</span>
                                </strong>
                            </h6>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control input-sm" value="1">
                        </div>
                        <div class="col-xs-2">
                            <button type="button" class="btn btn-link btn-xs">
                                <span class="glyphicon glyphicon-trash removeitemCarrinho" data-id="<?= $produto['Produtos']['id'] ?>"> </span>
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
        <?php } 
        }else{ ?>
            <b>Nenhum Produto adicionado ao carrinho</b>
        <?php }
        ?>
    </div>
</div>