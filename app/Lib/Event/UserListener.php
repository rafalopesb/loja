<?php
App::uses('CakeEventListener', 'Event');
App::uses('CakeEmail', 'Network/Email');
require_once(APP . 'Vendor' . DS . 'PHPMailer' . DS . 'PHPMailerAutoload.php');

class UserListener implements CakeEventListener {

    public function implementedEvents() {
    	    return ['Model.User.created' => 'sendConfirmationEmail'];
    }

    public function sendConfirmationEmail(CakeEvent $Event) {
            
            $mail = new PHPMailer;

            // $mail->SMTPDebug = 3;                               // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.live.com';                    // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;     
            $mail->From = 'rafalopesb@hotmail.com'; 		  // Seu e-mail
            $mail->FromName = 'Rafael';           // Enable SMTP authentication
            $mail->Username = 'rafalopesb@hotmail.com';             // SMTP username
            $mail->Password = '';                     // SMTP password
            //$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 25;                                    // TCP port to connect to

            $mail->setFrom('rafalopesb@hotmail.com', 'rafalopesb');

            $mail->addAddress('rafalopesb@hotmail.com', 'rafalopesb');     

            $mail->isHTML(true);

            $mail->Subject = 'Cadastro de usuário loja teste';

            $mail->Body = 'Testando eventos com cakephp';
            $s = $mail->send();
            
            return $s;
    }
}