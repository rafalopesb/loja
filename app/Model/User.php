<?php

App::uses('CakeEvent', 'Event');

class User extends AppModel {

    public function beforeSave($options = array()) {
        
        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
    }

    public function afterSave($created, $options = array()){
        parent::afterSave($created, $options);

        if ($created === true) {
            
            $Event = new CakeEvent('Model.User.created', $this, array(
                'id' => $this->id,
                'data' => $this->data[$this->alias]
            ));

            $this->getEventManager()->dispatch($Event);
        }
    }
}
?>