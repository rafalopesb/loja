<?php

class Produto extends AppModel {

    public $belongsTo = 'users';
    public $hasAndBelongsToMany = array(
        'Categoria' => array(
            'className' => 'Categoria',
            'joinTable' => 'categorias_produtos',
            'foreignKey' => 'produto_id',
            'associationForeignKey' => 'categoria_id',
            // 'unique' => true,
        ),
    );

    private $file_upload_info;
    private $file_name;

    public function __construct() {
        parent::__construct();
    }

    public function beforeSave($options = array()) {

        $this->data['Produto']['users_id'] = $_SESSION['Auth']['User']['id'];
        $this->data['Categoria']['id'] = $this->data['Produto']['categoria_id'];

        if ($this->data['Produto']['imagem']['error'] == 4) {
            unset($this->data['Produto']['imagem']);
            return true;
        }

        $this->file_upload_info = $this->data['Produto']['imagem'];
        $this->setFileNameToSave();
        $this->data['Produto']['imagem'] = $this->getFileNameToSave();

        return true;
    }

    public function afterSave($created, $options = []) {

        if (!empty($this->file_upload_info['tmp_name']) &&
            is_uploaded_file($this->file_upload_info['tmp_name'])) {

            $filename = basename($this->file_upload_info['name']);

            move_uploaded_file(
                $this->file_upload_info['tmp_name'],
                WWW_ROOT . 'img' . DS . 'produtos' . DS . $this->getFileNameToSave()
            );
        }

        return true;
    }

    public function beforeFind($query) {

        if (!empty($_SESSION['Auth']['User']['id'])) {
            $query['conditions'][$this->alias . '.users_id'] = $_SESSION['Auth']['User']['id'];
        }

        return $query;
    }

    public function getArraySelectCategorias($categorias) {

        $options_categorias = [];
        foreach ($categorias as $cat) {
            $options_categorias[$cat['Categorias']['id']] = $cat['Categorias']['nome'];
        }

        return $options_categorias;
    }

    private function getFileNameToSave() {
        return $this->file_name . '.' . pathinfo($this->file_upload_info['name'], PATHINFO_EXTENSION);
    }

    private function setFileNameToSave() {
        $path = $this->data['Produto']['imagem']['name'];

        $this->file_name = $this->data['Produto']['descricao'] . '_' . time();
    }
}
?>