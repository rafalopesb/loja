<?php 

    class Categoria extends AppModel{
        public $belongsTo = 'users';
        var $hasAndBelongsToMany = array(
            'Produto' => array(
                'className' => 'Produto',
                'joinTable' => 'categorias_produtos',
                'foreignKey' => 'categoria_id',
                'associationForeignKey' => 'produto_id'
            )
        );   

        public function __construct(){
            parent::__construct();
        }

        public function beforeFind($query) {
            
            if (!empty($_SESSION['Auth']['User']['id'])) {
                $query['conditions'][$this->alias . '.users_id'] = $_SESSION['Auth']['User']['id'];
            }
            return $query;
        }

        public function beforeSave($options = []){
            $this->data['Categoria']['users_id'] = $_SESSION['Auth']['User']['id'];
        }
    }
?>