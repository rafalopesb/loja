<?php
class Dashboard extends AppModel
{

    public function getScreenData()
    {
        $produtos = ClassRegistry::init('Produto');
        $categorias = ClassRegistry::init('Categorias');

        return array('produtos' => $produtos->find('all', array('limit' => 8, 'order' => 'Produto.id')),
            'categorias' => $categorias->find('all', array('limit' => 12, 'order' => 'Categorias.nome')));
    }
}
