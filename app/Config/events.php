<?php

App::uses('ClassRegistry', 'Utility');
App::uses('UserListener', 'Event');

$User = ClassRegistry::init('User');
$User->getEventManager()->attach(new UserListener());

/**
 * Para salvar Global
 */
// CakeEventManager::instance()->attach(new UserListener());
