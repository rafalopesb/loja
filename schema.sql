-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.7.23-0ubuntu0.18.04.1 - (Ubuntu)
-- OS do Servidor:               Linux
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para loja
CREATE DATABASE IF NOT EXISTS `loja` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `loja`;

-- Copiando estrutura para tabela loja.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `users_id` int(11) unsigned DEFAULT NULL,
  `icon` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_categorias_users` (`users_id`),
  CONSTRAINT `FK_categorias_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.categorias: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `nome`, `users_id`, `icon`) VALUES
	(1, 'Camisetas', 31, 'tags'),
	(5, 'Eletrônicos', 31, 'television'),
	(6, 'Jogos', 31, 'gamepad'),
	(9, 'Sapatos', 31, 'tag'),
	(10, 'Calças', 31, 'tag'),
	(11, 'Ingressos', 31, 'ticket'),
	(12, 'Carros', 31, 'car'),
	(13, 'Livros', 31, 'book');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.categorias_produtos
CREATE TABLE IF NOT EXISTS `categorias_produtos` (
  `produto_id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`produto_id`,`categoria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.categorias_produtos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias_produtos` DISABLE KEYS */;
INSERT INTO `categorias_produtos` (`produto_id`, `categoria_id`) VALUES
	(4, 1),
	(7, 6),
	(46, 10),
	(48, 5),
	(49, 9),
	(50, 5),
	(51, 6),
	(52, 13);
/*!40000 ALTER TABLE `categorias_produtos` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `preco` float unsigned DEFAULT NULL,
  `imagem` text,
  `frete_gratis` tinyint(1) unsigned DEFAULT '0',
  `users_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_produtos_users` (`users_id`),
  CONSTRAINT `FK_produtos_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.produtos: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` (`id`, `descricao`, `preco`, `imagem`, `frete_gratis`, `users_id`) VALUES
	(2, 'produto 1', 19.9, 'carro.png', 0, 1),
	(4, 'Camiseta Iron Maiden', 60, 'Camiseta Iron Maiden_1534333391.jpg', 1, 31),
	(5, 'TÃªnis Nike air max', 499.9, 'TÃªnis Nike air max_1534342039.jpg', 0, 31),
	(6, 'Celular Moto G6 Plus', 2000, 'Celular Moto G6 Plus_1534341969.jpg', 0, 31),
	(7, 'Jogo God of War - PS4', 189, 'Jogo God of War - PS4_1534341831.jpg', 0, 31),
	(8, 'Box Livros Senhor dos AnÃ©is', 90, 'Box Livros Senhor dos AnÃ©is_1534341876.jpg', 0, 31),
	(9, 'teste123', 19.99, 'carro.png', 1, 1),
	(18, 'teste123', 19.99, 'carro.png', 0, 31),
	(21, 'novo produto', 19.99, 'carro.png', 1, 31),
	(44, 'teste123', 123, 'carro.png', 0, 31),
	(45, 'teste123', 22, 'carro.png', 0, 31),
	(46, 'xx2', 1234, 'carro.png', 1, 31),
	(47, 'x2', 2, 'carro.png', 0, 31),
	(48, 'x', 12, 'carro.png', 0, 31);
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

-- Copiando estrutura para tabela loja.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(1) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela loja.users: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`) VALUES
	(1, 'teste123', 'bc4d2362c5a4c9d9aba88184c78a5af8568dae98', '2018-08-13 17:53:55', '2018-08-13 17:53:55'),
	(31, 'rafalopesb', 'bc4d2362c5a4c9d9aba88184c78a5af8568dae98', '2018-08-13 17:53:55', '2018-08-13 17:53:55'),
	(32, 'testeusuario', 'd19bdd659867547c99cb5e3b61ddf05adb949bf7', '2018-08-15 09:34:23', '2018-08-15 09:34:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
